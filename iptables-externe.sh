#!/bin/bash



### Tout Remettre à zéro
iptables -t nat -F
iptables -F
### Accepter les messages ICMP en entrée
iptables -A INPUT -p ICMP -j ACCEPT
### Tout accepter sur l'interface locale
iptables -A INPUT -i lo -j ACCEPT
### Suivi de connexions (stateful firewall)
# Sur les deux chaînes :
# - INPUT concernant la passerelle
# - FORWARD concernant les paquets routés
iptables -A INPUT   -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
### Règles de filtrage
### Quelques variables
DMZ='192.168.3.0/24'
PASSERELLE_EXT='10.10.41.123'
PASSERELLE_INT='192.168.3.1'
IF_EXTERNE='eth0'
IF_INTERNE='eth1'


# Passerelle
iptables -A INPUT -i $IF_INTERNE -s $PASSERELLE_INT -d $DMZ -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -i $IF_EXTERNE -s $PASSERELLE_EXT -j ACCEPT
## — Puis les flux
iptables -A INPUT -i $IF_INTERNE -s $DMZ -d $PASSERELLE_INT -p udp --dport 53 -j ACCEPT
iptables -A INPUT -i $IF_INTERNE -s $DMZ -d $PASSERELLE_INT -p tcp --dport 3128 -j ACCEPT

iptables -A FORWARD -i $IF_EXTERNE -d $DMZ -j DROP
iptables -A INPUT -i $IF_EXTERNE -d $PASSERELLE_EXT -p tcp --dport 22 -j ACCEPT
### Déni explicite par défaut
iptables -A INPUT -i $IF_INTERNE -j REJECT
iptables -A INPUT -i $IF_EXTERNE -j DROP
iptables -A FORWARD -i $IF_INTERNE -j REJECT
iptables -A FORWARD -i $IF_EXTERNE -j DROP
