#!/bin/bash




### Tout Remettre à zéro
iptables -t nat -F
iptables -F
### Accepter les messages ICMP en entrée
iptables -A INPUT -p ICMP -j ACCEPT
### Tout accepter sur l'interface locale
iptables -A INPUT -i lo -j ACCEPT
### Suivi de connexions (stateful firewall)
# Sur les deux chaînes :
# - INPUT concernant la passerelle
# - FORWARD concernant les paquets routés
iptables -A INPUT   -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
### Règles de filtrage
### Quelques variables
LAN='172.16.3.0/24'
DMZ='192.168.3.0/24'
PASSERELLE_EXT='192.168.3.2'
PASSERELLE_INT='172.16.3.1'
IF_EXTERNE='eth0'
IF_INTERNE='eth1'

## — La passerelle d'abord
iptables -A INPUT -i $IF_INTERNE -s $PASSERELLE_INT -d $LAN -j ACCEPT
iptables -A INPUT -i $IF_INTERNE -s $PASSERELLE_INT -d $DMZ -p udp --dport 53 -j ACCEPT
iptables -A INPUT -i $IF_INTERNE -s $PASSERELLE_INT -d $DMZ -p tcp --dport 3128 -j ACCEPT

## — Puis les flux
iptables -A INPUT -i $IF_INTERNE -s $LAN -d $PASSERELLE_INT -j REJECT
iptables -A INPUT -i $IF_INTERNE -s $LAN -d $DMZ -p udp --dport 53 -j ACCEPT
iptables -A INPUT -i $IF_INTERNE -s $LAN -d $DMZ -p tcp --dport 3128 -j ACCEPT

iptables -A FORWARD -i $IF_EXTERNE -s $DMZ -d $LAN -j DROP
iptables -A INPUT -i $IF_EXTERNE -s $DMZ -d $PASSERELLE_EXT -j DROP

### Déni explicite par défaut
iptables -A INPUT -i $IF_INTERNE -j REJECT
iptables -A INPUT -i $IF_EXTERNE -j DROP
iptables -A FORWARD -i $IF_INTERNE -j REJECT
iptables -A FORWARD -i $IF_EXTERNE -j DROP
